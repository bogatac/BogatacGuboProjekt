Mi smo ovaj projekt prvo namjeravali izraditi u Laravelu i Bootstrapu, ali smo se prebacili na Django zato 
jer smo htjeli naučiti programirati u Pythonu, pošto već znamo programirati u PHP-u.
Aplikacija za pregled bolesti po simptomima.
Funkcionira vrlo jednostavno.

Postoje tri vrste korisnika;
    Bolesnik, tj korisnik koji želi zatražiti pregled.
    On kada se registrira i ulogira ima mogućnosti pregleda svih lječnika ili svih bolesti.
    Kada dođe do liste bolesti može ih filtrirati po nazivu ili simptomima. 
    Ukoliko klikne na bolest, pišu osnovne informacije o bolesti te lječnik koji je zadužan za lječenje/pregled te bolesti. 
    Kada klikne na lječnika dostupne su mu informacije o tom lječniku te lokacija 
    i marker na mapi koristeći google maps. Također korisnik može zatražiti pregled. 
    Tamo mu se nudi forma koja mu omogućuje 
    da upiše naziv pregleda, status zatražio pregled i samu bolest(Nažalost ovdje nam baca listu svih bolesti, nismo znali kako napraviti restrikciju toga), Korisnik
    bi trebao odabrati bolest koja je povezana sa lječnikom. Tada se zahtjev prosljeđuje lječniku. Kada lječnik prihvati pregled to će korisnik vidjeti na moji pregledi.
    
    
Drugi korisnik je lječnik. Njegova mogućnost je da kada ode na listu neprihvaćenih pregleda koji su vezani za njega, tj zahtjevi koje je korisnik podnjeo prema njemu. Tamo može promjeniti
status pregleda sa zatraženog na prihvaćen i također može odrediti datum i vrijeme pregleda.


Treči korisnik je Admin. On ima pristup backendu preko django backenda ili unosu i mjenjanju podataka preko frontenda. Kada se stvara lječnik admin upisuje podatke te odabire sa liste 
registriranih korisnika osobu koja će biti lječnik. također se kod unosa bolesti odabire lječnik.
    
Aplikacija se pokreće tako da je na računalu instaliran python 3.8.3 te pip za django, treba skinuti fileove projekta.
također je potrebno putem pipa instalirati virtualenv putem komande pip install virtualenv.
Zatim se u mapi u kojoj će se pokretati projekt napravi virtual enviroment putem komande virtualenv .(treba staviti ovaj razmak i točku).
Nakon toga se kreira u toj istoj mapi još jedna mapa(recimo src) i unutra se postave fileovi projekta koji su skinuti.
Kada ste sa cmd-om ili powershellom u root folderu gdje su mape lib i scripts od virtualenva pokrene se komanda ./Scripts/activate 
Time ulazite u virtualni enviroment. Nakon toga se piše komanda cd src te nakon toga pip install -r requirements.txt
Nakon toga se aplikacija pokreće sa komandom py manage.py runserver
Aplikaciji se može pristupiti na localhost:8000
Korisnici za upotrebu su:

Admin - username:Admin password:Bolnica123

David Gubo(lječnik) - username: dgubo password:Bolnica123

Alen Alencic(lječnik) - username: aalencic password:Bolnica123

Bolesnik - username: Bolesnik password:Bolnica123

Slobodno možete napraviti nove putem registracije i dodjeljivanja korisnika kod unosa lječnika putem admin računa.
