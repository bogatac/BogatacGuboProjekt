from django.contrib import admin
from django.contrib.auth.models import Permission

# Register your models here.
from .models import *

class BolestAdmin(admin.ModelAdmin):
	list_display = ['naziv', 'simptom', 'simptom2', 'simptom3']
	list_filter = ['naziv']
	search_fields = ['naziv']

class LjecnikAdmin(admin.ModelAdmin):
	list_display = ['ime', 'prezime', 'telefon', 'lokacija','Korisnik']
	list_filter = ['ime', 'prezime', 'telefon', 'lokacija']
	search_fields = ['ime', 'prezime', 'telefon', 'lokacija']

admin.site.register(Bolest, BolestAdmin)
admin.site.register(Simptom)
admin.site.register(Simptom2)
admin.site.register(Simptom3)
admin.site.register(Ljecnik,LjecnikAdmin)
admin.site.register(Pregled)

