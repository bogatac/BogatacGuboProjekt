from django.urls import path
from . import views

urlpatterns = [
path('', views.home, name='home'),
path('bolesti/<int:pk>/', views.BolestDetailView.as_view(), name='detalj'),
path('bolesti/', views.BolestListView.as_view(), name='bolesti'),
path('bolest/<int:pk>/brisi/', views.BolestDelete.as_view(), name='bolest-brisi'),
path('bolest/novi', views.BolestCreate.as_view(), name='bolest-novi'),
path("register/", views.register, name="register"),
path('registered/', views.home, name='registered'),
path('ljecnici/', views.LjecnikListView.as_view(), name='ljecnici'),
path('ljecnik/<int:pk>',views.LjecnikDetailView.as_view(), name='ljecnik-opis'),
path('ljecnik/<int:pk>/brisi/', views.LjecnikDelete.as_view(), name='ljecnik-brisi'),
path('ljecnik/novi', views.LjecnikCreate.as_view(), name='ljecnik-novi'),
path('simptom/novi', views.SimptomCreate.as_view(), name='simptom-novi'),
path('simptom2/novi', views.Simptom2Create.as_view(), name='simptom2-novi'),
path('simptom3/novi', views.Simptom3Create.as_view(), name='simptom3-novi'),
path('simptomi/', views.simptomi, name='simptomi'),
path('simptom/<int:pk>/brisi/', views.SimptomDelete.as_view(), name='simptom-brisi'),
path('simptom2/<int:pk>/brisi/', views.Simptom2Delete.as_view(), name='simptom2-brisi'),
path('simptom3/<int:pk>/brisi/', views.Simptom3Delete.as_view(), name='simptom3-brisi'),
path('bolest/<int:pk>/update', views.UpdateBolest.as_view(), name='bupdate'),
path('ljecnik/<int:pk>/update', views.UpdateLjecnik.as_view(), name='ljupdate'),
path('simptom/<int:pk>/update', views.UpdateSimptom.as_view(), name='supdate'),
path('simptom2/<int:pk>/update', views.UpdateSimptom2.as_view(), name='s2update'),
path('simptom3/<int:pk>/update', views.UpdateSimptom3.as_view(), name='s3update'),
path('pregled/<int:pk>/add/', views.PregledAdd.as_view(), name='pg'),
path('mpregled/', views.PregledKorisnik.as_view(), name='m-pregled'),
path('lpregled/', views.PregledLjecnik.as_view(), name='l-pregled'),
path('lpregled/<int:pk>/update', views.PregledUpdate.as_view(), name='lupdate'),
path('user/<int:pk>', views.UserDetail.as_view(), name='user'),

]