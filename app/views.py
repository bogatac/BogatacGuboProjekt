from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from .models import *
from .forms import *
from django.views import View
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .filters import BolestFilter
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import get_object_or_404
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import permission_required

# Create your views here.
@login_required
def home(request):
    br_bolesti = Bolest.objects.all().count()
    br_ljecnika = Ljecnik.objects.all().count()
    context = {
		'br_bolesti': br_bolesti,
        'br_ljecnika': br_ljecnika,
		
	}
    return render(request, 'app/home.html', context)
class SecretPage(TemplateView):
    template_name = 'app/home.html'

def mapa(request):
    return render(request, 'app/mapa.html')

class BolestListView(LoginRequiredMixin, ListView):
	model = Bolest
	template_name = 'app/bolest_list.html'
	

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context ['filter'] = BolestFilter(self.request.GET, queryset= self.get_queryset())
		return context


class BolestDetailView(LoginRequiredMixin, DetailView):
	model = Bolest
	template_name = 'app/bolest_detail.html'

class LjecnikListView(LoginRequiredMixin, ListView):
    model = Ljecnik
    template_name = 'app/ljecnik_list.html'

class LjecnikDetailView(LoginRequiredMixin, DetailView):
    model = Ljecnik
    template_name = 'app/ljecnik_detail.html'


def register(response):
    if response.method == "POST":
        form = RegisterForm(response.POST)
        if form.is_valid():
            form.save()

        return redirect('/registered')
    else:
        form = RegisterForm()

    return render(response, "registration/register.html", {"form":form})


class LjecnikDelete(PermissionRequiredMixin, DeleteView):
    model = Ljecnik
    success_url = reverse_lazy('ljecnici')
    permission_required = 'is_superuser'


class LjecnikCreate(PermissionRequiredMixin, CreateView):
    model = Ljecnik
    form_class = LjecnikForm
    permission_required = 'is_superuser'


class BolestDelete(PermissionRequiredMixin, DeleteView):
    model = Bolest
    success_url = reverse_lazy('bolesti')
    permission_required = 'is_superuser'


class BolestCreate(PermissionRequiredMixin, CreateView):
    model = Bolest
    form_class = BolestForm
    permission_required = 'is_superuser'


class SimptomCreate(PermissionRequiredMixin, CreateView):
    model = Simptom
    form_class = SimptomForm
    permission_required = 'is_superuser'

class Simptom2Create(PermissionRequiredMixin, CreateView):
    model = Simptom2
    form_class = Simptom2Form
    permission_required = 'is_superuser'

class Simptom3Create(PermissionRequiredMixin, CreateView):
    model = Simptom3
    form_class = Simptom3Form
    permission_required = 'is_superuser'

@permission_required('is_superuser')
def simptomi(request):
    simptom = Simptom.objects.all()
    simptom2 = Simptom2.objects.all()
    simptom3 = Simptom3.objects.all()
    return render(request, 'app/simptom_list.html', {"Simptom":simptom,"Simptom2":simptom2,"Simptom3":simptom3})



class SimptomDelete(PermissionRequiredMixin, DeleteView):
    model = Simptom
    success_url = reverse_lazy('simptomi')
    permission_required = 'is_superuser'

class Simptom2Delete(PermissionRequiredMixin, DeleteView):
    model = Simptom2
    success_url = reverse_lazy('simptomi')
    permission_required = 'is_superuser'

class Simptom3Delete(PermissionRequiredMixin, DeleteView):
    model = Simptom3
    success_url = reverse_lazy('simptomi')
    permission_required = 'is_superuser'


class UpdateBolest(PermissionRequiredMixin, UpdateView):

   
    model = Bolest
    form_class = BolestForm
    permission_required = 'is_superuser'

    def get_success_url(self):
        return '/bolesti'


class UpdateLjecnik(PermissionRequiredMixin, UpdateView):

   
    model = Ljecnik
    form_class = LjecnikForm
    permission_required = 'is_superuser'

    def get_success_url(self):
        return '/ljecnici'

class UpdateSimptom(PermissionRequiredMixin, UpdateView):

   
    model = Simptom
    form_class = SimptomForm
    permission_required = 'is_superuser'

    def get_success_url(self):
        return '/simptomi'

class UpdateSimptom2(PermissionRequiredMixin, UpdateView):

   
    model = Simptom2
    form_class = Simptom2Form
    permission_required = 'is_superuser'

    def get_success_url(self):
        return '/simptomi'

class UpdateSimptom3(PermissionRequiredMixin, UpdateView):

   
    model = Simptom3
    form_class = Simptom3Form
    permission_required = 'is_superuser'

    def get_success_url(self):
        return '/simptomi'


class PregledAdd(CreateView):

   
    model = Pregled
    form_class = PregledForm
    
    

  

    def form_valid(self, form):
        form.instance.ljec_id = self.kwargs.get('pk')
        form.instance.use = self.request.user
        
        print(form.cleaned_data)
        return super(PregledAdd, self).form_valid(form)
        
    def get_success_url(self):
        return '/bolesti'



class PregledKorisnik(LoginRequiredMixin, ListView):
    model = Pregled
    template_name ='app/pregledkorisnik.html'
    paginate_by = 10
    
    def get_queryset(self):
        return Pregled.objects.filter(use=self.request.user).filter(status__exact='p').order_by('dat_pregleda')

class PregledLjecnik(LoginRequiredMixin, ListView):
    model = Pregled
    template_name ='app/pregledljecnik.html'
    paginate_by = 10
    def get_queryset(self):
        return Pregled.objects.filter(ljec_id__Korisnik_id=self.request.user).filter(status__exact='z').order_by('dat_pregleda')


class PregledUpdate(PermissionRequiredMixin, UpdateView):

   
    model = Pregled
    form_class = PregledLForm
    permission_required = 'app.pregled'
    
    def get_success_url(self):
        return '/bolesti'

class UserDetail(LoginRequiredMixin, DetailView):
    model = User
    template_name = 'app/user_detail.html'