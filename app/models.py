from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.

class Bolest(models.Model):
	naziv = models.CharField(max_length = 100)
	simptom = models.ForeignKey('Simptom', on_delete=models.SET_NULL, null=True, verbose_name = 'Prvi Simptom')
	simptom2 = models.ForeignKey('Simptom2', on_delete=models.SET_NULL, null=True, verbose_name = 'Drugi Simptom')
	simptom3 = models.ForeignKey('Simptom3', on_delete=models.SET_NULL, null=True, verbose_name = 'Treći Simptom')
	ljecnik = models.ForeignKey('Ljecnik', on_delete=models.SET_NULL, null= True, verbose_name= 'Lječnik')

	class Meta:
		verbose_name_plural = "Bolesti"

	def __str__(self):
		return self.naziv

	def get_absolute_url(self):
		return reverse('detalj', args=[str(self.id)])


class Simptom(models.Model):
	naziv = models.CharField(max_length = 100)
	class Meta:
		verbose_name_plural = "Prvi Simptomi"


	def __str__(self):
		return self.naziv
	def get_absolute_url(self):
		return reverse('simptomi')

class Simptom2(models.Model):
	naziv = models.CharField(max_length = 100)
	

	class Meta:
		verbose_name_plural = "Drugi Simptomi"


	def __str__(self):
		return self.naziv

	def get_absolute_url(self):
		return reverse('simptomi')

class Simptom3(models.Model):
	naziv = models.CharField(max_length = 100)

	class Meta:
		verbose_name_plural = "Treći Simptomi"


	def __str__(self):
		return self.naziv

	def get_absolute_url(self):
		return reverse('simptomi')

class Ljecnik(models.Model):
	ime = models.CharField(max_length= 20)
	prezime = models.CharField(max_length= 20)
	opis = models.TextField(max_length = 200)
	telefon = models.CharField(max_length= 10, blank = True)
	oib = models.CharField(max_length= 11, verbose_name = 'OIB')
	lokacija = models.CharField(max_length= 50)
	Korisnik = models.OneToOneField(User, on_delete=models.SET_NULL, null= True)

	class Meta:
		verbose_name_plural = "Lječnici"

	def __str__(self):
		return '{0}, {1}'.format(self.prezime, self.ime)
	def get_absolute_url(self):
		return reverse('ljecnik-opis', args=[str(self.id)])

class Pregled(models.Model):
    naziv = models.CharField(max_length = 100, null = True, )
    ljec = models.ForeignKey('Ljecnik', on_delete=models.CASCADE, null=True, verbose_name = 'Lječnik', blank = True) 
    dat_pregleda = models.DateField(null=True, blank=True)
    bolest = models.ForeignKey('Bolest', on_delete=models.CASCADE, null=True, blank=True, verbose_name = 'Bolest')
    use = models.ForeignKey(User, on_delete=models.CASCADE ,null=True,verbose_name = 'Korisnik' )
    vrijeme = models.TimeField(null= True, blank = True) 



    STATUS = (
        ('z', 'Zatražio pregled'),
        ('p', 'Prihvaćen pregled'),
        
        
    )

    status = models.CharField(
        max_length=1,
        choices=STATUS,
        blank=False,
        default='z',
        )

    class Meta:
        verbose_name_plural = "Pregledi"
        permissions = (('pregled', 'pregled'),)
    def __str__(self):
    	return self.naziv
        
	