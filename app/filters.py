import django_filters
from .models import *

class BolestFilter(django_filters.FilterSet):
	class Meta:
		model = Bolest
		fields =  {
		'naziv':['icontains'],
		'simptom':['exact'],
		'simptom2':['exact'],
		'simptom3':['exact'],
		}


