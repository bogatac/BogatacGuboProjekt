from django import forms
from .models import *
from django.contrib.auth.forms import UserCreationForm

STATUS = (
            ('z', 'Zatražio pregled'),)


class RegisterForm(UserCreationForm):
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ["username", "email", "password1", "password2"]
        help_texts = {
            'username': None,
            'email': None,
        }

class LjecnikForm(forms.ModelForm):
	class Meta:
		model = Ljecnik
		fields = ['ime', 'prezime', 'opis','telefon', 'oib', 'lokacija','Korisnik']


class BolestForm(forms.ModelForm):
	class Meta:
		model = Bolest
		fields = ['naziv','simptom', 'simptom2', 'simptom3','ljecnik']

class SimptomForm(forms.ModelForm):
    class Meta:
        model = Simptom
        fields = ['naziv']

class Simptom2Form(forms.ModelForm):
    class Meta:
        model = Simptom2
        fields = ['naziv']

class Simptom3Form(forms.ModelForm):
    class Meta:
        model = Simptom3
        fields = ['naziv']

class PregledForm(forms.ModelForm):
    status = forms.ChoiceField(choices= STATUS)
    class Meta:
        model = Pregled
        
        fields = ['naziv', 'dat_pregleda', 'bolest']
        exclude = ('dat_pregleda',)
        dat_pregleda = forms.DateField(
            widget=forms.DateInput(format='%d.%m.%Y'),
            input_formats=('%d.%m.%Y'),
            label = 'Datum Pregleda'
            )

class PregledLForm(forms.ModelForm):
    class Meta:
        model = Pregled
        fields = ['dat_pregleda','vrijeme','status']
        dat_pregleda = forms.DateField(
            widget=forms.DateInput(format='%d.%m.%Y'),
            input_formats=('%d.%m.%Y'),
            label = 'Datum Pregleda'
            )

        
            
        
  